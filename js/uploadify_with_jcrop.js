(function ($) {

  Drupal.behaviors.uploadify_jcrop = {
    attach: function (context, settings) {
      var jcrop_api, boundx, boundy, boundz;
      var aspectRatio = Drupal.settings.uploadify_jcrop.aspectratio;

      $("#ujUpload").uploadify({          
        'formData' : {
          'timestamp' : Drupal.settings.uploadify_jcrop.passTime,
          'token'     : Drupal.settings.uploadify_jcrop.timeToken
        },
        'auto' : true,
        'multi' : false,
        'uploadLimit' : Drupal.settings.uploadify_jcrop.uploadLimit,
        'buttonText': Drupal.settings.uploadify_jcrop.buttontext,
        'width':'120',
        'height':'24',
        'removeCompleted'	: false,
        'queueID':'upimage-queue',
        'queueSizeLimit':10,
        'swf' : Drupal.settings.uploadify_jcrop.modulePath + '/lib/uploadify/uploadify.swf',
        'uploader' : Drupal.settings.basePath + 'uj/upload',
        'fileTypeExts' : Drupal.settings.uploadify_jcrop.filetypeexts,
        'fileSizeLimit' : Drupal.settings.uploadify_jcrop.filesizelimit,
        'onFallback':function(){
          alert(Drupal.t('You do not install flash, you can not upload pictures! Please install flash and try again.'));
        },
        'onUploadSuccess' : function(file, data, response) {
          var msg = $.parseJSON(data);
          if( msg.code == 1 ){
              $('#upimage').css({"visibility":"hidden","height":"1","width":"0"});
              $('#upimage-upfinal').show();
              $("#upimage-origimage").val(msg.origimage);
              $("#upimage-fid").val(msg.fid);
              $("#target").attr("src",msg.image);
              $("#preview").attr("src",msg.image);
              $("#aspect").val(msg.aspect);
              $('#target').Jcrop({
                      minSize: [20,20],
                      setSelect: [0,0,Drupal.settings.uploadify_jcrop.preview_w,Drupal.settings.uploadify_jcrop.preview_h],
                      onChange: updatePreview,
                      onSelect: updatePreview,
                      onSelect: updateCoords,
                      aspectRatio: aspectRatio
                  },
                  function(){
                      var bounds = this.getBounds();
                      boundx = bounds[0];
                      boundy = bounds[1];
                      jcrop_api = this;
                  }
              );
              $('#previewdiv').css('width', Drupal.settings.uploadify_jcrop.preview_w * aspectRatio + 'px');
              showCropButton();
          } else {
              alert(Drupal.t('Upload failed'));
          }
        }
      });

      function updateCoords(c){
          $('#upimage-x').val(c.x);
          $('#upimage-y').val(c.y);
          $('#upimage-w').val(c.w);
          $('#upimage-h').val(c.h);
      };

      function updatePreview(c){
          var w = Drupal.settings.uploadify_jcrop.preview_w * aspectRatio;
          var h = Drupal.settings.uploadify_jcrop.preview_h;
          if (parseInt(c.w) > 0){
              var z = w / c.w;
              $('#preview').css({
                  width: Math.round(z * boundx) + 'px',
                  height: Math.round(z * boundy) + 'px',
                  marginLeft: '-' + Math.round(z * c.x) + 'px',
                  marginTop: '-' + Math.round(z * c.y) + 'px'
              });
          }
      };

      function showCropButton() {
          $('#submitbtn').html('<button type="button" id="cropbutton" class="am-btn am-btn-default am-radius"><i class="fa fa-check"></i>' + Drupal.t('Confirm Crop') +'</button>');
          $("#cropbutton").click(function() {
              var fid = $("#upimage-fid").val();
              var img = $("#upimage-origimage").val();
              var x = $("#upimage-x").val();
              var y = $("#upimage-y").val();
              var w = $("#upimage-w").val();
              var h = $("#upimage-h").val();
              $.ajax({
                  type: "POST",
                  url: Drupal.settings.basePath + 'uj/resize',
                  data: {"fid":fid,"img":img,"x":x,"y":y,"w":w,"h":h, "type":"crop"},
                  dataType: "json",
                  success: function(msg){
                      if( msg.code == 1 ){
                          $('#upimage-finalimage').val(msg.image);
                          $('#upimage-upfinal').html('<img src="'+ msg.image +'" />');
                          $('#submitbtn').hide();
                      } else {
                          alert(Drupal.t('Crop failed'));
                      }
                  }
              });
          });
      }

      function fanzhuan() {
        aspectRatio = 1 / aspectRatio;
        jcrop_api.setOptions({aspectRatio:aspectRatio});
        $('#previewdiv').css('width', 100 * aspectRatio + 'px');
      }
    }
  };

})(jQuery);
