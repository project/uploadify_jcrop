(function ($) {

  Drupal.behaviors.uploadify_jcrop = {
    attach: function (context, settings) {
      $("#ujUpload").uploadify({          
          'formData' : {
            'timestamp' : Drupal.settings.uploadify_jcrop.passTime,
            'token'     : Drupal.settings.uploadify_jcrop.timeToken
          },
          'auto' : true,
          'multi' : true,
          'uploadLimit' : Drupal.settings.uploadify_jcrop.uploadLimit,
          'buttonText': Drupal.settings.uploadify_jcrop.buttontext,
          'width':'120',
          'height':'24',
          'removeCompleted'	: false,
          'queueID':'upimage-queue',
          'queueSizeLimit': Drupal.settings.uploadify_jcrop.uploadLimit,
          'swf' : Drupal.settings.uploadify_jcrop.modulePath + '/lib/uploadify/uploadify.swf',
          'uploader' : Drupal.settings.basePath + 'uj/upload',
          'fileTypeExts' : Drupal.settings.uploadify_jcrop.filetypeexts,
          'fileSizeLimit' : Drupal.settings.uploadify_jcrop.filesizelimit,

          'itemTemplate':'<div id="${fileID}" class="uploadify-queue-item">\<a class="ify-cancel" href="javascript:jQuery(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')">' + Drupal.t('Delete') +'</a>\<p class="ify-data"></p>\<p class="ify-fileName">${fileName}</p>\</div>',
          'overrideEvents': ['onUploadSuccess'],
          'onFallback':function(){
            alert(Drupal.t('You do not install flash, you can not upload pictures! Please install flash and try again.'));
          },
          'onUploadSuccess' : function(file, data, response) {
            $('#upimage-queue').show();
            var msg = $.parseJSON(data);
            var img='<img src="'+msg.image+'" width="100" height="100" /><input type="hidden" name="upimage_images[]" value="'+msg.image+'" readonly />';
            $('#' + file.id).find('.ify-data').html(img);
          }
      });   
    }
  };

})(jQuery);
